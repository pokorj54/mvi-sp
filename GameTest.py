#!/bin/python3

import os
import re
import subprocess
import time
import sys

if len(sys.argv) < 3:
    print("Two arguments expected - run this program as ./program game update_scores")
    print("game - folder in positions as well as argument for PNSSolver")
    print("update_scores - 0 or 1 - whether the score should be saved for comparrison if it beats the current best score")
    exit(1)

GAME_NAME = sys.argv[1]
UPDATE_COUNTS = sys.argv[2].lower() in ("yes", "true", "t", "1")
SOLVER_PATH = "./exe/PNSSolver"
TOTAL_RUN_TIME = 0

def read_file(filename):
    result = None
    if(os.path.isfile(filename)):
        with open(filename) as file:
            result = file.read()
    return result

def write_file(filename, contents):
    with open(filename, "w") as reference:
            reference.write(contents)

def run_program(position_name):
    print("=================================================")
    print(position_name)
    input_game = open(position_name + ".pos")
    reference_output = read_file(position_name + ".ref")
    reference_no_nodes = read_file(position_name + ".meta")
    reference_no_nodes = int(reference_no_nodes) if reference_no_nodes!= None else None
    start = time.time()
    p = subprocess.Popen([SOLVER_PATH, GAME_NAME], stdin=input_game, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    program_output = [x.decode('UTF-8') for x in p.communicate()]
    game_result = program_output[0]
    program_output[1] = program_output[1].split('\n')
    nodes_opened = program_output[1][0]
    if len(program_output[1]) > 1:
        depth_searched = program_output[1][1]
    end = time.time()
    global TOTAL_RUN_TIME
    TOTAL_RUN_TIME += end - start
    print("time: {}".format(end - start))
    assert reference_output == game_result or reference_output == None, "Program outputed {}, but in reference was {}".format(game_result, reference_output) 
    print("result: {}".format(game_result))
    print("reference: {} \t current: {}".format(reference_no_nodes, int(nodes_opened)))
    if len(program_output[1]) > 1:
        print("depth: ", depth_searched)
    if(reference_output == None or reference_no_nodes == None):
        write_file(position_name + ".ref", game_result)
        write_file(position_name +  ".meta", nodes_opened)
        print("Writing results")
    elif(int(reference_no_nodes) > int(nodes_opened) and UPDATE_COUNTS):
        write_file(position_name +  ".meta", nodes_opened)
        print("Updating results")


def run_on_all_positions():

    game = GAME_NAME  
    prefix = './positions/{}/'.format(game)
    entries = os.listdir(prefix)
    for game_type in ["easy", "medium", "hard"]:
        regex = re.compile("{}..\\.pos".format(game_type))
        positions = list(filter(regex.search, entries))
        positions.sort()

        for position in positions:
            run_program(prefix + position[:-4])


run_on_all_positions()

print("Total runtime: {}".format(TOTAL_RUN_TIME))