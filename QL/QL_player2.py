import tensorflow as tf
import numpy as np
from player import Player
import common
import random
import plots

class QLPlayer2(Player):
    def __init__(self, sizes, funs = None, pretrain = [], r = 0.95):
        self.model = tf.keras.Sequential()
        self.sizes = sizes
        for size in sizes:
            self.model.add(tf.keras.layers.Dense(size))
        self.model.compile(optimizer='sgd', loss='mean_squared_error')
        self.funs = funs
        if self.funs == None:
            self.funs = [lambda x: x]
        for board, q_vals in pretrain:
            self.fit_q(board,q_vals)
        self.r = r
        self.gamma = 0.8
        self.training = True
        self.confidence = []

    def has_state(self):
        return True

    def to_string(self):
        return 'QL2_' + '_'.join([str(s) for s in self.sizes])


    def save_state(self, file_path):
        self.model.save_weights(file_path)
        plots.moving_avg_plot(self.confidence, file_path + '.png', 'confidence of QL')


    def load_state(self, file_path):
        self.model.load_weights(file_path)


    def predict_q(self, board):
        return self.model.predict(
            np.array([board.ravel()]), verbose=0).reshape(board.shape)

    def fit_q(self, board, q_values):
        self.model.fit(
            np.array([board.ravel()]), np.array([q_values.ravel()]), verbose=0)

    def new_game(self, training):
        self.training = training
        self.moves = []
        self.board_history = []
        self.q_history = []

    def move(self, game):
        board = game.board
        q_values = self.predict_q(board)
        temp_q = q_values.copy()
        available_moves = game.available_moves()
        available_moves_table = common.indices_to_table(board.shape, available_moves)
        temp_q[available_moves_table == 0] = temp_q[available_moves_table != 0].min() -1  # no illegal moves

        move = np.unravel_index(np.argmax(temp_q), board.shape)
        if self.training and random.random() > self.r:
            move = tuple(random.choice(game.available_moves()))

        assert move in available_moves
        self.board_history.append(board.copy())
        self.q_history.append(q_values)
        self.moves.append(move)
        return move

    def reward(self, reward_value, f):
        i = 0
        while i < len(self.board_history)-1:
            new_q = f(self.q_history[i].copy())
            new_q[self.moves[i]] += self.gamma *(new_q[self.moves[i+1]] - new_q[self.moves[i]]) 
            self.fit_q(f(self.board_history[i]), new_q)
            i += 1
        new_q = f(self.q_history[i].copy())
        new_q[self.moves[i]] = reward_value
        self.fit_q(f(self.board_history[i]), new_q)

    def game_ended(self, result: int):
        self.update_confidence()
        if self.training:
            for f in self.funs:
                self.reward(result, f)

    def update_confidence(self):
        conf = 0
        for i in range(len(self.board_history)):
            conf += self.q_history[i][self.moves[i]]
        conf = conf/len(self.board_history)
        self.confidence.append(conf)
