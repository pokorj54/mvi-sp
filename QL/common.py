import numpy as np

"""one if the cell is in indices otherwise zero"""
def indices_to_table(shape, indices):
    res = np.zeros(shape=shape)
    for index in indices:
        res[tuple(index)] = 1
    return res
