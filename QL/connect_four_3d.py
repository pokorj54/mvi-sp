import numpy as np
from game_n3 import GameN3

class ConnectFour3D(GameN3):
    def __init__(self, n):
        super().__init__(n)

    
    def name(self):
        return "cf3d{}".format(self.n)


    def available_moves(self):
        result = []
        for i in range(4):
            for j in range(4):
                for k in range(4):
                    if self.board[i][j][k] == 0:
                        result.append((i,j,k))
                        break
        return result
