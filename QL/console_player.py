from game import Game
from player import Player

class ConsolePlayer(Player):
    def move(self, board: Game):
        print(board.board)
        return tuple([int(i) for i in input().strip().split(' ')])


    def to_string(self):
        return 'ConsolePlayer'
