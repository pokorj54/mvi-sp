import numpy as np
from game import Game

class GameN2(Game):
    def __init__(self, n):
        self.n = n
        self.board = np.zeros(shape=(n, n))
        self.k = 2

    def name(self):
        return "{}^2".format(self.n)

    def available_moves(self):
        return np.argwhere(self.board == 0)

    def check_game_end(self):
        best = max(list(self.board.sum(axis=0)) +    # columns
                list(self.board.sum(axis=1)) +    # rows
                [self.board.trace()] +            # main diagonal
                [np.fliplr(self.board).trace()],  # other diagonal
                key=abs)
        if abs(best) == self.board.shape[0]:  # assumes square board
            return np.sign(best)  # winning player, +1 or -1
        if self.available_moves().size == 0:
            return 0
        return None

    def play(self, move, player):
        self.board[tuple(move)] = player
