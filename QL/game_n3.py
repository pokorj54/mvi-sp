import numpy as np
from game import Game
import line3


class GameN3(Game):
    def __init__(self, n):
        self.n = n
        self.board = np.zeros(shape=(n, n, n))
        self.all_lines = line3.get_all_lines(n)
        self.k = 3

    def name(self):
        return "{}^3".format(self.n)

    def available_moves(self):
        return np.argwhere(self.board == 0)


    def play(self, move, player):
        self.board[tuple(move)] = player

    def check_game_end(self):
        default = False, None

        def line_won(prev, pos):
            a, b, c = pos
            val = self.board[a][b][c]
            if prev == None:
                prev = val
            if val == 0 or prev != val:
                return 0
            return val
        for line in self.all_lines:
            r = line3.exec_fun_line(line, self.n, line_won, None)
            if r in [-1, 1]:
                return r
        return 0 if len(self.available_moves()) == 0 else None
