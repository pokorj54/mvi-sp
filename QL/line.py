import line2
import line3


def get_all_lines(n,k):
    if k == 2:
        return line2.get_all_lines(n)
    if k == 3:
        return line3.get_all_lines(n)
    raise Exception("not supported")



def exec_fun_line(line, n, k, func, default_elem):
    if k == 2:
        return line2.exec_fun_line(line, n, func, default_elem)
    if k == 3:
        return line3.exec_fun_line(line, n, func, default_elem)
    raise Exception("not supported")


def get_game_heatmap(n,k):
    if k == 2:
        return line2.get_game_heatmap(n)
    if k == 3:
        return line3.get_game_heatmap(n)
    raise Exception("not supported")