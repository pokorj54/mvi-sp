import numpy as np

def is_canonical(i, j, n):
    isConstant = i < n and j < n  and i > -1 and j > -1
    lastMinus1 = False
    lastMinus1 = (True if i == -1 else (False if i == n else lastMinus1))
    lastMinus1 = (True if j == -1 else (False if j == n else lastMinus1))
    return not isConstant and lastMinus1


def get_all_lines(n):
    lines = []
    for a in range(-1, n+1):
        for b in range(-1, n+1):
            if is_canonical(a, b, n):
                lines.append((a, b))
    return lines

# func has to have this shape func(acc:R, (a,b,c)) -> R and default element is of type R


def exec_fun_line(line, n, func, default_elem):
    a, b = line
    apos, bpos = a, b
    for _ in range(n):
        if a == -1:
            apos += 1
        if a == n:
            apos -= 1
        if b == -1:
            bpos += 1
        if b == n:
            bpos -= 1
        default_elem = func(default_elem, (apos, bpos))
    return default_elem

def get_game_heatmap(n):
    all_lines = get_all_lines(n)
    board = np.zeros((n,n))
    def add_one_line(board, pos):
        board[pos] += 1
        return board
    for line in all_lines:
        board = exec_fun_line(line, n, add_one_line, board)
    return board