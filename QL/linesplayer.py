from player import Player
from game import Game
import line as ln
import numpy as np
import common
import random

class LinesPLayer(Player):
    def __init__(self, token, all_lines, n, k, line_values, r):
        self.token = token
        self.all_lines = all_lines
        self.n = n
        self.k = k
        self.line_vals = line_values
        self.r = r
        

    def move(self, game:Game):
        board = game.board
        scores = np.zeros(shape = board.shape)
        def get_line_count(line_count, pos):
            val = int(board[pos])
            line_count[val] += 1
            return line_count

        def add_to_cell(score, pos):
            scores[pos] += score
            return score

        for line in self.all_lines:
            empty = {-1:0, 0:0, 1:0}
            count = ln.exec_fun_line(line, self.n, self.k, get_line_count, empty)
            count_tuple = (count[self.token], count[self.token * -1])
            score = self.line_vals[count_tuple] if count_tuple in self.line_vals else 0
            ln.exec_fun_line(line, self.n, self.k, add_to_cell, score)
            
        available_moves = game.available_moves()
        available_moves_table = common.indices_to_table(board.shape, available_moves)
        scores[available_moves_table == 0] = scores.min() - 1
        move = np.unravel_index(np.argmax(np.ravel(scores)), board.shape)
        while random.random() > self.r:
            scores[tuple(move)] = scores.min() - 1
            scores[available_moves_table == 0] = scores.min() - 1
            move = np.unravel_index(np.argmax(np.ravel(scores)), board.shape)
        assert move in game.available_moves()
        return move
 

    def to_string(self):
        return 'LinePLayer_' + str(self.r)