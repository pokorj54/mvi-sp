from game import Game
import game_n2
import game_n3
import connect_four_3d

from player import Player
from random_player import RandomPlayer
from weighted_random_player import WeightedRandomPlayer
from linesplayer import LinesPLayer
from console_player import ConsolePlayer
from QL_player import QLPlayer
from QL_player2 import QLPlayer2
import line
import plots

import random 
import traceback

# inspired by https://github.com/ajschumacher/ajschumacher.github.io/blob/7901598f018d2ca979e1b7ad85f418cf10a6b49b/20191103-q_learning_tic_tac_toe_briefly/q_learning_tic_tac_toe.ipynb

MODELS_FOLDER = '.models/'

def play(board: Game, players:dict, training):
    players[1].new_game(training)
    players[-1].new_game(training)
    player = +1
    game_end = board.check_game_end()
    while game_end is None:
        move = players[player].move(board)
        board.play(move, player)
        # print(board.board)
        game_end = board.check_game_end()
        player *= -1  # switch players
    # print(board.board)
    for player_sign in players:
        players[player_sign].game_ended(game_end * player_sign)
    return game_end

def log_epoch(game_fact, player1, player2, epoch, tmp_wins, tmp_loses, epoch_n, results):
    game_name = game_fact().name()
    name = MODELS_FOLDER + game_name + '_' + player1.to_string() + '_' + player2.to_string() + '_' + str(epoch)
    if player1.has_state():
        player1.save_state(name + '_1')
    if player2.has_state():
        player2.save_state(name + '_2')
    print('e:', epoch, 'w:', tmp_wins, 'l:', tmp_loses, 'd:', epoch_n-tmp_loses-tmp_wins)
    plots.moving_winrates(results, name + '.png', title = game_name + ': ' + player1.to_string() + ' vs: ' + player2.to_string())

def measure(game_fact, player1:Player, player2:Player, epochs, epoch_n, training):
    wins = 0
    loses = 0
    results = []
    for epoch in range(epochs):
        tmp_wins = 0
        tmp_loses = 0
        for _ in range(epoch_n):
            p = play(game_fact(), {+1: player1, -1: player2}, training)
            # print(p)
            if p == 1:
                tmp_wins +=1
            elif p == -1:
                tmp_loses += 1
            results.append(p)
        wins += tmp_wins
        loses += tmp_loses
        log_epoch(game_fact, player1, player2, epoch, tmp_wins, tmp_loses, epoch_n, results)
    print(wins, loses, epochs*epoch_n - wins -loses)

def get_game_fact(game:str, *args):
    if game == "cf3d":
        return lambda : connect_four_3d.ConnectFour3D(4)
    separators=['**', '^']
    for separator in separators:
        if separator in game:
            n,k = tuple(game.split(separator))
            n, k = int(n), int(k)
            if k == 2:
                return lambda : game_n2.GameN2(n)
            if k == 3:
                return lambda : game_n3.GameN3(n)
    raise Exception("No game selected")
    

def get_player(name, n, k, player_token, *args):
    if name == 'random':
        return RandomPlayer()
    if name == 'weighted':
        return WeightedRandomPlayer(line.get_game_heatmap(n, k))
    if name == 'lines':
        # TODO generate somehow
        line_vals = {
            (0,0): 10,
            (0,1): 5,
            (0,2): 100,
            (0,3): 1000,
            (1,0): 10,
            (2,0): 100,
            (3,0): 1000,
        }
        return LinesPLayer(player_token, line.get_all_lines(n, k), n, k, line_vals, float(args[0]))
    if name == 'console':
        return ConsolePlayer()
    # generate somehow?
    symmetries = [
        lambda x: x, 
        # lambda x: np.rot90(x), 
        # lambda x: np.rot90(np.rot90(x)),
        # lambda x: np.rot90(np.rot90(np.rot90(x))),
        # lambda x: np.flip(x, axis=0), 
        # lambda x: np.flip(np.rot90(x), axis=0), 
        # lambda x: np.flip(np.rot90(np.rot90(x)), axis=0),
        # lambda x: np.flip(np.rot90(np.rot90(np.rot90(x))), axis=0), 
        ]
    pretrain = [
        #(np.zeros(shape=(4,4,4)), line.get_game_heatmap(4,3))
    ]
    if name == 'ql':
        return QLPlayer([int(a) for a in args], symmetries, pretrain)
    if name == 'ql2':
        return QLPlayer2([int(a) for a in args], symmetries, pretrain)
    raise Exception("No player selected")

class CommandLine:
    def __init__(self):
        self.handle_command = {
            'train': self.train,
            'save': self.save,
            'load': self.load,
            'player': self.create,
            'game': self.game, 
            'play': self.play
        }
        self.players = {
            1:None,
            -1:None
        }
        self.game = None

    def train(self, epochs, length):
        measure(self.game_fact, self.players[1], self.players[-1], int(epochs), int(length), True)

    def play(self, epochs, length):
        measure(self.game_fact, self.players[1], self.players[-1], int(epochs), int(length), False)

    def load(self, player, file_name):
        self.players[int(player)].load_state(MODELS_FOLDER + file_name)

    def save(self, player, file_name):
        self.players[int(player)].save_state(MODELS_FOLDER + file_name)

    def create(self, player, name, *args):
        self.players[int(player)] = get_player(name, self.n, self.k, int(player), *args)
    
    def game(self, game, *args):
        self.game_fact = get_game_fact(game, *args)
        tmp = self.game_fact()
        self.k = tmp.k
        self.n = tmp.n


    def run(self):
        print('Commands:', [key for key in self.handle_command])
        print('>', end=' ')
        command = input().strip()
        while command != 'bye':
            if command != "":
                cmds = command.split(' ')
                try:
                    handle = self.handle_command[cmds[0]]
                    handle(*cmds[1:])
                except:
                    traceback.print_exc()
            print('>', end=' ')
            command = input().strip()


def main():
    random.seed(1)
    cl = CommandLine()
    cl.run()

main()