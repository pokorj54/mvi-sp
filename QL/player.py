from game import Game

class Player:
    def new_game(self, training):
        pass

    def move(self, board: Game):
        pass

    # 1: win, 0:draw, -1:lost
    def game_ended(self, result: int):
        pass

    def to_string(self):
        pass

    def has_state(self):
        return False

    def save_state(self, file_path):
        pass

    def load_state(self, file_path):
        pass
