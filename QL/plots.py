import matplotlib.pyplot as plt
import matplotlib.ticker



def moving_avg(data, size):
    return [sum(data[i-size:i])/size for i in range(size, len(data) + 1)]


def moving_percentage(data, value=+1, size=100):
#def moving(data, value=+1, size=100):
    binary_data = [x == value for x in data]
    # this is wasteful but easy to write...
    return [sum(binary_data[i-size:i])/size for i in range(size, len(data) + 1)]
    binary_data = [x == value for x in data]
    return moving_avg(binary_data, size)

def moving_winrates(results, filename = None, title='Moving average of game outcomes',
         first_label='First Player Wins', second_label='Second Player Wins', draw_label='Draw'):
    size = len(results)//20+1
    x_values = range(size, len(results) + 1)
    first = moving_percentage(results, value=+1, size=size)
    second = moving_percentage(results, value=-1, size=size)
    draw = moving_percentage(results, value=0, size=size)
    first, = plt.plot(x_values, first, color='red', label=first_label)
    second, = plt.plot(x_values, second, color='blue', label=second_label)
    draw, = plt.plot(x_values, draw, color='grey', label=draw_label)
    plt.xlim([0, len(results)])
    plt.ylim([0, 1])
    plt.title(title)
    plt.legend(handles=[first, second, draw], loc='best')
    ax = plt.gca()
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter(xmax=1))
    ax.xaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.ylabel(f'Rate over trailing window of {size} games')
    plt.xlabel('Game Number')
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)
    plt.clf()

def moving_avg_plot(results, filename = None, label='label'):
    size = len(results)//20+1
    x_values = range(size, len(results) + 1)
    first = moving_avg(results, size=size)
    first, = plt.plot(x_values, first, color='red', label=label)
    plt.xlim([0, len(results)])
    plt.ylim([min(min(results), 0), max(results)])
    plt.title('Moving average of ' + label)
    plt.legend(handles=[first], loc='best')
    ax = plt.gca()
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter(xmax=1))
    ax.xaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.ylabel(f'Rate over trailing window of {size} games')
    plt.xlabel('Game Number')
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)
    plt.clf()

