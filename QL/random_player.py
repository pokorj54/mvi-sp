import random
from player import Player
from game import Game

class RandomPlayer(Player):
    def move(self, board:Game):
        return random.choice(board.available_moves())

    
    def to_string(self):
        return 'RandomPlayer'