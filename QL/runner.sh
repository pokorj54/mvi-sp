#!/bin/bash

for i in echo "${@:1}"
do
    o="${i/.input/.output}"
    python3 main.py -u <"$i" >"$o" 2>&1 &
done
