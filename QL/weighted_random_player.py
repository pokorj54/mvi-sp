from player import Player
from game import Game
import random

class WeightedRandomPlayer(Player):
    def __init__(self, weights):
        self.weights = weights

    # at most linear
    def move(self, board:Game):
        am = board.available_moves()
        am = [tuple(i) for i in am] 
        s = sum([self.weights[move] for move in am])
        r = random.randint(0, s-1)
        i = 0
        while self.weights[am[i]] <= r:
            r -= self.weights[am[i]]
            i += 1
        return am[i]
    
    
    def to_string(self):
        return 'WeeightedRandomPlayer'
