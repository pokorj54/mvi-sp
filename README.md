# NI-MVI - semestral project

### An attempt to solve 3D Connect Four

Jan Pokorný

## Connect Four 3D

The positional game Connect Four 3D is played on a 4x4x4 board by two players.
Players alternate choosing an unfilled column and putting a stone of their color to the lowest possible position in that column.
Player that achieves to have 4 stones of their color on a line (horizontal, vertical, diagonal) wins the game.


## Goal

Create a deep learning agent capable of playing 3D connect four.
If possible use the agent to create an exact exact algorithm for solving the game.

## How to use

The only requirements are `numpy` and `tensorflow`.

To start the program run `python3 QL/main.py`.
That opens a command line interface with the following commands:

- `game <name>` - selects the game that will be player - either `n^2`, `n^3` for tic tac toes on  a square or cube with side length `n` or `cf3d` for Connect Four 3D 
- `player <player_id> <name> [extra agent args ...]` - selects the agent with name `<name>` to be the player with given id. Id 1 means the first player and id -1 is the second player, no other ids are allowed. The available players are.
  - `random` - agent that playes random available move
  - `weighted` - agent that weights the moves based of how many lines goes through each cell and selects one accordingly.
  - `lines <r>` - scores each cell based on each line that goes through it. The bigger the advantage is gained or the bigger the thread stopped by placing a stone, the higher the score is given. The parameter `r` determines the randomness of this player.
  - `console` - player that reads the moves from stdin 
  - `ql [shape of NN]` - Q-learning agent 1, shape of NN is given as space separated integers
  - `ql2 [shape of NN]` - Q-learning agent 2 - better, , shape of NN is given as space separated integers
- `save <player_id> <name>` - saves the state of player with the given player id as `<name>` 
- `load <player_id> <name>`- load the state of player with the given player id from `<name>` 
- `play <epochs> <games_per_epoch>` - for each epoch plays `<games_per_epoch>` many games and prints the results to console.
- `train <epochs> <games_per_epoch>` - same as `play` and additionally agents that are able to learn will learn 
- `bye` - closes the console

The commands can be automated by sending file on `stdin`. The used scripts are saved in `QL/scripts`.
The final model is `QL/.models/cf3d_ql2_weighted_lines_043`. It can be loaded with the program by  `load 1 QL/.models/cf3d_ql2_weighted_lines_043`, but first the correct player has to be selected - `player 1 ql2 64 128 128 128 128 128 128 64`.


## Reports

Milestone report can be viewed [here](MVI_Milestone.pdf).
The final report can be viewed [here](report.pdf).
