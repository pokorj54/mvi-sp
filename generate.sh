#!/bin/bash
folder="./positions/$1/generated"
mkdir -p $folder
while [ true ]
do
    R=$((10 + $RANDOM % 20))
    echo $1 $R
    ./exe/GenerateGame $1 $R  >game.txt
    timeout "10s" ./exe/PNSSolver $1  <game.txt 1>game.ref 2>game.meta
    if [ $? = 0 ]
    then
        res=$(cat game.meta)
        echo $res
        if [[ $res =~ ^[01][^0-9][01] ]]
        then
            continue
        fi
        timecreated=$(date +%s)
        echo "saved $timecreated"
        mv game.txt $folder/game$timecreated.pos
        mv game.ref $folder/game$timecreated.ref
        mv game.meta $folder/game$timecreated.meta
    else
        continue
    fi
done
