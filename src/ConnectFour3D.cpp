#include "ConnectFour3D.hpp"
#include "utils.hpp"
#include "QubicLikeUtils.hpp"

#include <algorithm>
#include <cmath>

ConnectFour3D::CF3DBoard ConnectFour3D::lines[76];

ConnectFour3D::CF3DBoard ConnectFour3D::pointLines[64][7];

ConnectFour3D::ConnectFour3D(CF3DBoard firstPlayerStones, CF3DBoard secondPlayerStones) : firstPlayerStones(firstPlayerStones), secondPlayerStones(secondPlayerStones) {
    // no two stones are at the same cell
    assert((firstPlayerStones & secondPlayerStones) == 0);
    // players have the right amount of stones
    int firstPCount = countOnes(firstPlayerStones);
    int secondPCount = countOnes(secondPlayerStones);
    assert(firstPCount == secondPCount || firstPCount == secondPCount + 1);
}

bool ConnectFour3D::operator==(const ConnectFour3D &other) const {
    return firstPlayerStones == other.firstPlayerStones && secondPlayerStones == other.secondPlayerStones;
}

bool ConnectFour3D::operator!=(const ConnectFour3D &other) const {
    return !(*this == other);
}

bool ConnectFour3D::operator<=(const ConnectFour3D &other) const {
    if (this->firstPlayerStones != other.firstPlayerStones) {
        return this->firstPlayerStones <= other.firstPlayerStones;
    }
    return this->secondPlayerStones <= other.secondPlayerStones;
}

bool ConnectFour3D::operator>=(const ConnectFour3D &other) const {
    return other <= *this;
}

bool ConnectFour3D::operator<(const ConnectFour3D &other) const {
    return !(*this >= other);
}

bool ConnectFour3D::operator>(const ConnectFour3D &other) const {
    return !(*this <= other);
}

GameState ConnectFour3D::getState() const {
    if (hasWinningLine(firstPlayerStones)) {
        return FirstWon;
    }
    if (hasWinningLine(secondPlayerStones) || (firstPlayerStones | secondPlayerStones) == (CF3DBoard)-1) {  // -1 = 0xff...ff, it is sufficient only to check second player
        return SecondWonOrDraw;
    }
    return Open;
}

GameState ConnectFour3D::getState(const ConnectFour3D &parent) const {
    CF3DBoard first = this->firstPlayerStones ^ parent.firstPlayerStones;
    CF3DBoard second = this->secondPlayerStones ^ parent.secondPlayerStones;
    bool played_first = first != 0;
    if (played_first) {
        assert(second == 0);
        return hasWinningLine(this->firstPlayerStones, first, pointLines) ? FirstWon : Open;
    } else {
        assert(first == 0);
        bool draw = (firstPlayerStones | secondPlayerStones) == (CF3DBoard)-1;
        return hasWinningLine(this->secondPlayerStones, second, pointLines) | draw ? SecondWonOrDraw : Open;
    }
}

bool ConnectFour3D::isValid() const{
    CF3DBoard stones = (firstPlayerStones | secondPlayerStones);
    if ((stones | (stones >> 16)) != stones){
        return false;
    }
    return true;
}

int ConnectFour3D:: stonesPlayed() const{
    return countOnes(firstPlayerStones | secondPlayerStones);
}

ConnectFour3D ConnectFour3D::loadGame(istream &is) {
    ConnectFour3D game;
    for (int i = 0; i < 64; ++i) {
        char c;
        is >> c;
        if (is.bad() || is.eof()) {
            throw ParseException("Expected more characters");
        }
        game.firstPlayerStones = game.firstPlayerStones << 1;
        game.secondPlayerStones = game.secondPlayerStones << 1;
        switch (c) {
            case 'x':
                game.firstPlayerStones += 1;
                break;
            case 'o':
                game.secondPlayerStones += 1;
                break;
            case '-':
                break;
            default:
                throw ParseException("Unexpected character");
        }
    }
    if(!game.isValid()){
        throw ParseException("Invalid board");
    }
    return game;
}

bool ConnectFour3D::firstPlayerPlays() const {
    return countOnes(firstPlayerStones) == countOnes(secondPlayerStones);
}

ostream &operator<<(ostream &os, const ConnectFour3D &q) {
    ConnectFour3D::CF3DBoard bit = 1UL << 63;  // bit that is currently searched
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            for (int k = 0; k < 4; ++k) {
                if ((bit & q.firstPlayerStones) == bit) {
                    os << 'x';
                } else if ((bit & q.secondPlayerStones) == bit) {
                    os << 'o';
                } else {
                    os << '-';
                }
                bit = bit >> 1;
            }
            os << endl;
        }
        os << endl;
    }
    return os;
}

int ConnectFour3D::getChildrenCount() const {
    return 16 - countOnes((firstPlayerStones | secondPlayerStones) & 0xffff'0000'0000'0000);
}

void fill_column(ConnectFour3D::CF3DBoard & board, ConnectFour3D::CF3DBoard stone){
    stone = stone | (stone >> 16) | (stone << 16);
    stone = stone | (stone >> 32) | (stone << 32); 
    board = board | stone;
}

//same as qubic for now
int gradeMovesCF(int stone_index, QubicBoard firstPlayerStones, QubicBoard secondPlayerStones, bool plays_first) {
    int score = 100;
    QubicBoard nextPlayer = plays_first ? firstPlayerStones : secondPlayerStones;
    QubicBoard opponent = plays_first ? secondPlayerStones : firstPlayerStones;
    bool isomd = is_on_main_diagonal(1UL << stone_index);
    int numLines = isomd ? 7 : 4;

    const int score_transformation[16] = {// row is player, column is opponent, -0 marks tha it is not possible
                                          5, 0, 0, 1000,
                                          15, -2, -2, -0,
                                          100, -2, -0, -0,
                                          10000, -0, -0, -0};
    for (int i = 0; i < numLines; ++i) {
        QubicBoard line = ConnectFour3D::pointLines[stone_index][i];
        int nextPlayerCount = countOnes(nextPlayer & line);
        int opponentCount = countOnes(opponent & line);
        int combined = nextPlayerCount * 4 + opponentCount;
        score += score_transformation[combined];
    }

    (void)firstPlayerStones;
    (void)secondPlayerStones;
    return (isomd == !plays_first) ? score + 10 : score;
}

vector<ConnectFour3D> ConnectFour3D::generateChildren() const {
    CF3DBoard stones = firstPlayerStones | secondPlayerStones;
    bool plays_first = countOnes(stones) % 2 == 0;
    int n_childs = getChildrenCount();

    vector<pair<CF3DBoard, int>> validMoves = vector<pair<CF3DBoard, int>>(n_childs);
    CF3DBoard stone = 0x1;
    CF3DBoard put_mask = stones;
    int current_child = 0;
    for (int i = 0; i < 64; ++i) {
        if ((stone & put_mask) == 0) {
            int points = gradeMovesCF(i, firstPlayerStones, secondPlayerStones, plays_first);
            validMoves[current_child++] = {stone, points};
            fill_column(put_mask, stone);
        }
        stone = stone << 1;
    }

    sort(validMoves.begin(), validMoves.end(), [](const pair<CF3DBoard, int> &a, const pair<CF3DBoard, int> &b) { return a.second > b.second; });

    vector<ConnectFour3D> children;
    for (auto &gradedBoard : validMoves) {
        CF3DBoard move = gradedBoard.first;
        if (plays_first) {
            children.push_back(ConnectFour3D(firstPlayerStones | move, secondPlayerStones));
        } else {
            children.push_back(ConnectFour3D(firstPlayerStones, secondPlayerStones | move));
        }
    }
    return children;
}

void ConnectFour3D::generateLines() {
    int count = 0;
    // ordinary lines
    // left to right
    generateOffsetedParallelLines(0xf, 4, 16, lines + count);
    count += 16;
    // forward backward
    ConnectFour3D::CF3DBoard mask = 0x1111;
    for (int i = 0; i < 4; ++i) {
        generateOffsetedParallelLines(mask, 1, 4, lines + count);
        count += 4;
        mask = mask << 16;
    }
    // up down
    generateOffsetedParallelLines(0x0001000100010001, 1, 16, lines + count);
    count += 16;
    // face diagonals
    // face up
    generateOffsetedParallelLines(0x8421, 16, 4, lines + count);
    count += 4;
    generateOffsetedParallelLines(0x1248, 16, 4, lines + count);
    count += 4;
    // face left
    generateOffsetedParallelLines(0x1000010000100001, 1, 4, lines + count);
    count += 4;
    generateOffsetedParallelLines(0x0001001001001000, 1, 4, lines + count);
    count += 4;
    //face forward
    generateOffsetedParallelLines(0x0008000400020001, 4, 4, lines + count);
    count += 4;
    generateOffsetedParallelLines(0x0001000200040008, 4, 4, lines + count);
    count += 4;
    // main diagonals
    generateOffsetedParallelLines(0x8000040000200001, 0, 1, lines + count);
    count += 1;
    generateOffsetedParallelLines(0x1000020000400008, 0, 1, lines + count);
    count += 1;
    generateOffsetedParallelLines(0x0008004002001000, 0, 1, lines + count);
    count += 1;
    generateOffsetedParallelLines(0x0001002004008000, 0, 1, lines + count);
    count += 1;
    assert(count == 76);

    CF3DBoard stone = 1;
    for (int i = 0; i < 64; ++i) {
        int current_line = 0;
        for (int j = 0; j < 76; ++j) {
            if ((stone & lines[j]) == stone) {
                pointLines[i][current_line++] = lines[j];
            }
        }
        assert(current_line == (is_on_main_diagonal(stone) ? 7 : 4));
        stone = stone << 1;
    }
}
