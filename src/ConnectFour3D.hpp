#pragma once

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <vector>

#include "GameState.hpp"
#include "ParseException.hpp"

using namespace std;

struct ConnectFour3D {
    using CF3DBoard = uint64_t;

    CF3DBoard firstPlayerStones;   // x
    CF3DBoard secondPlayerStones;  // o
    ConnectFour3D() : firstPlayerStones(0), secondPlayerStones(0) {}
    ConnectFour3D(CF3DBoard firstPlayerStones, CF3DBoard secondPlayerStones);

    bool operator==(const ConnectFour3D &other) const;

    bool operator!=(const ConnectFour3D &other) const;

    bool operator<=(const ConnectFour3D &other) const;

    bool operator>=(const ConnectFour3D &other) const;

    bool operator<(const ConnectFour3D &other) const;

    bool operator>(const ConnectFour3D &other) const;

    GameState getState() const;
    GameState getState(const ConnectFour3D &parent) const;
    int getChildrenCount() const;
    vector<ConnectFour3D> generateChildren() const;
    bool firstPlayerPlays() const;
    int stonesPlayed() const;
    bool isValid() const;

    /* board mapped to bits (index)

    63 62 61 60 - top
    59 58 57 56
    55 54 53 52
    51 50 49 48
    
    47 46 45 44
    43 42 41 40
    39 38 37 36
    35 34 33 32
    
    31 30 29 28
    27 26 25 24
    23 22 21 20
    19 18 17 16
    
    15 14 13 12
    11 10 09 08
    07 06 05 04
    03 02 01 00 - bottom 
    */
    static ConnectFour3D loadGame(istream &is);

    friend std::ostream &operator<<(std::ostream &os, const ConnectFour3D &q);

    static CF3DBoard lines[76];
    static CF3DBoard pointLines[64][7];

    static void generateLines();
};

namespace std {
template <>
struct hash<ConnectFour3D> {
    std::size_t operator()(const ConnectFour3D &q) const {
        ConnectFour3D::CF3DBoard rotatedSecondPlayerStones = (q.secondPlayerStones << (sizeof(ConnectFour3D::CF3DBoard) / 2)) |
                                                      (q.secondPlayerStones >> (sizeof(ConnectFour3D::CF3DBoard) / 2));
        return std::hash<ConnectFour3D::CF3DBoard>{}(q.firstPlayerStones) ^ std::hash<ConnectFour3D::CF3DBoard>{}(rotatedSecondPlayerStones);
    }
};
}  // namespace std
