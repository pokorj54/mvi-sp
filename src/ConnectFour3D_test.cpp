#include "ConnectFour3D.hpp"
#include "utils.hpp"

#include <assert.h>
#include <iomanip>

int main(void) {
    ConnectFour3D empty;
    vector<ConnectFour3D> children = empty.generateChildren();
    assert(children.size() == 16);
    assertUniqueElements(children);
    assertAllChildState(children, Open);
    return 0;
}