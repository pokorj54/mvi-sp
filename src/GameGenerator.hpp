#pragma once

#include <random>
#include "GameState.hpp"
#include <cassert>

struct GameGenerator{
    GameGenerator():gen(std::random_device()()){}
    template <typename Game>
    Game generate(const Game & game, int moves){
        for(int i = 0; i < 10; ++i){
            Game copy = game;
            int movesDone= 0;
            while(movesDone < moves && game.getState() == GameState::Open){
                vector<Game> children = copy.generateChildren();
                std::uniform_int_distribution<> distrib(0,  children.size()-1);
                copy = children[distrib(gen)];
                ++movesDone;
            }
            if(movesDone == moves){
                return copy;
            }
        }
        assert(0 && "not able to generatate game");
    }
    private:
        std::mt19937 gen;
};