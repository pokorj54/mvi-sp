#include "ConnectFour3D.hpp"
#include "Qubic.hpp"
#include "utils.hpp"
#include "GameGenerator.hpp"

#include <assert.h>
#include <iomanip>

int main(void) {
    GameGenerator gg;
    ConnectFour3D cfempty;
    assert(gg.generate(cfempty, 0) == cfempty);
    assert(gg.generate(cfempty, 1) != cfempty); 

    for(int i = 0; i < 7;++i){
        assert(gg.generate(cfempty, i).stonesPlayed() == i); 
    }
    assert(gg.generate(cfempty, 5) != gg.generate(cfempty, 5));
    return 0;
}