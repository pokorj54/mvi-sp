#include "GameState.hpp"

const char* GameStateToString(GameState gameState) {
    switch (gameState) {
        case Open:
            return "Open";
        case FirstWon:
            return "First player wins";
        case SecondWonOrDraw:
            return "Second player wins or draws";
    }
    throw std::invalid_argument("Unknown gamestate");
}
