#pragma once

#include <stdexcept>

enum GameState { Open,
                 FirstWon,
                 SecondWonOrDraw };

const char* GameStateToString(GameState gameState);
