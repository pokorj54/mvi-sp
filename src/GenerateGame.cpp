#include <iostream>
#include "Qubic.hpp"
#include "ConnectFour3D.hpp"
#include "GameGenerator.hpp"
#include <string>

using namespace std;

int main(int argc, char ** argv){
    if(argc != 3){
        cerr << "Run this program as ./program game nomber_of_moves" << endl;
        return 0; 
    }
    string game = string(argv[1]);
    int k = atoi(argv[2]);
    GameGenerator gg;
    if(game == "qubic"){
        cout << gg.generate(Qubic(), k) << endl;
    } else if(game == "cf"){
        cout << gg.generate(ConnectFour3D(), k) << endl;
    } else{
        cerr << "expected cf or qubic" << endl;
        return 1;
    }
    return 0;
}