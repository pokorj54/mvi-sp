#include <cstdint>
#include <vector>
#include <iostream>

#include "SaturatedUInt.hpp"

#include "TranspositionTable/TranspositionTableHashed.hpp"

using namespace std;

template <typename Game>
struct Node {
   private:
    Node(const Game &game, bool isAndNode, Node *parent) :
    pn(SaturatedUInt::INFTY), dn(SaturatedUInt::INFTY), game(game), isAndNode(isAndNode), parent(parent), depth(-1) {}


   public:
    Node(const Game &game) : Node(game, !game.firstPlayerPlays(), nullptr) {}
    SaturatedUInt pn, dn;  // TODO calculate whats enough size(type) for particular game
    // pn = 0 - win for 1st player
    // dn = 0 - draw or win for 2nd player
    const Game game;
    const bool isAndNode;
    Node *const parent;
    vector<Node> children;
    int depth;

        
    bool operator==(const Node &other) const {
        return this->game == other.game;
    }

    bool operator!=(const Node &other) const {
        return !(*this == other);
    }

    bool isExpanded() const {
        return children.size() != 0;
    }
    GameState getGameState() const {
        if (pn == 0) {
            return FirstWon;
        } else if (dn == 0) {
            return SecondWonOrDraw;
        }
        return Open;
    }

    void setProofAndDisproofNumbers(TranspositionTableHashed<Game> & tt) {
        GameState tt_result = tt.getGameState(this->game);
        if (tt_result != Open) {
            setProofAndDisproofNumbers(tt_result);
            depth =tt.getVal(this->game);
            return;
        }
        if (this->isExpanded()) { /* interior node */
            if (this->isAndNode) {
                this->pn = 0;
                this->dn = SaturatedUInt::INFTY;
                int max_depth = -1;
                for (Node &c : this->children) {  // check speed
                    this->pn = this->pn + c.pn;
                    this->dn = std::min(this->dn, c.dn);
                    max_depth = std::max(max_depth, c.depth);
                    if(c.dn == 0){
                        this->depth = c.depth+1;
                    }
                }
                if(this->pn == 0){
                    this->depth = max_depth+1;
                }
            } else { /* OR node */
                this->pn = SaturatedUInt::INFTY;
                this->dn = 0;
                int max_depth = -1;
                for (Node &c : this->children) {
                    this->dn = this->dn + c.dn;
                    this->pn = std::min(this->pn, c.pn);
                    max_depth = std::max(max_depth, c.depth);
                    if(c.pn == 0){
                        this->depth = c.depth+1;
                    }
                }
                if(this->dn == 0){
                    this->depth = max_depth+1;
                }
            }
        } else { /* terminal node or non-terminal leaf */
            if (this->parent != nullptr) {
                setProofAndDisproofNumbers(this->game.getState(this->parent->game));
            } else {
                setProofAndDisproofNumbers(this->game.getState());
            }
            // it is terminal leaf
            if(this->getGameState() != Open){
                depth = 0;
            }
        }
    }

    void setProofAndDisproofNumbers(GameState state) {
        switch (state) {
            case SecondWonOrDraw:
                this->pn = SaturatedUInt::INFTY;
                this->dn = 0;
                break;
            case FirstWon:
                this->pn = 0;
                this->dn = SaturatedUInt::INFTY;
                break;
            case Open:
                this->pn = 1;
                this->dn = 1;
                break;
            default:
                throw std::invalid_argument("Unknown gamestate");
        }
    }

    Node *updateAncestors(TranspositionTableHashed<Game> & tt) {
        Node *current = this;
        while (true) {
            SaturatedUInt oldpn = current->pn;
            SaturatedUInt olddn = current->dn;
            current->setProofAndDisproofNumbers(tt);
            // adding to TT if the position was solved
            if (current->getGameState() != Open) {
                tt.add(current->game, current->getGameState(), current->depth);
            }
            if ((current->pn == oldpn && current->dn == olddn) || current->parent == nullptr) {
                return current;
            }
            current = current->parent;
        }
    }

    Node *selectMostProvingNode() {
        Node *current = this;
        while (current->isExpanded()) {
            SaturatedUInt value = SaturatedUInt::INFTY;
            Node *best = nullptr;
            if (current->isAndNode) {
                for (Node &child : current->children) {
                    if (child.getGameState() != Open) {  // is this sound?
                        continue;
                    }
                    if (value > child.dn) {
                        best = &child;
                        value = child.dn;
                    }
                }
            } else { /* OR node */
                for (Node &child : current->children) {
                    if (child.getGameState() != Open) {  // Is this sound?
                        continue;
                    }
                    if (value > child.pn) {
                        best = &child;
                        value = child.pn;
                    }
                }
            }
            if (best == nullptr) {
                cerr << "No MPN found" << endl;
                cerr << *current;
                for (Node &child : current->children) {
                    cerr << child;
                }
                throw logic_error("No MPN found");
            }
            current = best;
        }
        return current;
    }
    
    void expand( TranspositionTableHashed<Game> & tt) {
        vector<Game> children_games = this->game.generateChildren();
        for (Game &child_game : children_games) {
            Node child = Node(child_game, !this->isAndNode, this);
            child.setProofAndDisproofNumbers(tt);
            children.push_back(child);
            if ((this->isAndNode && child.dn == 0) || (!this->isAndNode && child.pn == 0)) {
                break;
            }
        }
    }
    template <typename G>
    friend std::ostream &operator<<(std::ostream &os, const Node<G> &n);
};

template <typename Game>
ostream &operator<<(ostream &os, const Node<Game> &n) {
    os << "Type: " << (n.isAndNode ? "AND" : "OR") << endl;
    os << "pn: " << (uint)n.pn << endl;
    os << "dn: " << (uint)n.dn << endl;
    os << "State: " << GameStateToString(n.getGameState()) << endl;
    os << n.game;
    return os;
}


template <typename Game>
void PNS(Node<Game> &root, TranspositionTableHashed<Game> & tt) {
    int c = 0;
    root.setProofAndDisproofNumbers(tt);
    Node<Game> *current = &root;
    while (root.pn != 0 && root.dn != 0) {
        Node<Game> *mostProving = current->selectMostProvingNode();
        mostProving->expand(tt);
        current = mostProving->updateAncestors(tt);
        ++c;
    }
    cerr << c << endl;
}