#include <iomanip>
#include <iostream>
#include <string>

#include "GameState.hpp"
#include "PNS.hpp"
#include "Qubic.hpp"
#include "ConnectFour3D.hpp"
#include "TranspositionTable/TranspositionTableHashed.hpp"

template <typename Game>
void play(){
    Game::generateLines();
    Game game;
    try {
        game = Game::loadGame(std::cin);
    } catch (const ParseException& e) {
        std::cerr << e.getMessage() << endl;
        throw;
    }

    Node<Game> root(game);
    TranspositionTableHashed<Game>  tt(100'000'000);
    PNS(root, tt);
    cout << GameStateToString(root.getGameState()) << endl;
    cerr << root.depth << endl;
}

int main(int argc, char ** argv) {
    if(argc != 2){
        cerr << "Expected 1 argument, run this program as ./program game" << endl; 
        return 1;
    }
    std::string game = argv[1];
    if(game == "qubic"){
        play<Qubic>();
    }else if(game == "cf"){
        play<ConnectFour3D>();
    } else{
        cerr << "unknown game, expected qubic or cf" << endl;
        return 1;
    }
    return 0;
}