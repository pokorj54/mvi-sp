#include "PNS.hpp"
#include "TranspositionTable/TranspositionTableHashed.hpp"
#include "Qubic.hpp"
#include <cassert>

int main(void) {
    Qubic::generateLines();
    TranspositionTableHashed<Qubic> tt;

    Node<Qubic> gameNodeEasy = Node<Qubic>(Qubic(0x7, 0x70));
    PNS(gameNodeEasy, tt);
    assert(gameNodeEasy.getGameState() == FirstWon);
    assert(gameNodeEasy.depth == 1);

    Node<Qubic> gameNode = Node<Qubic>(Qubic(0xc84081b180410000, 0xb01c4010909000));
    gameNode.expand(tt);
    gameNode.setProofAndDisproofNumbers(tt);
    assert(gameNode.getGameState() == SecondWonOrDraw);
    return 0;
}