#pragma once

#include <exception>

class ParseException : public std::exception {
    const char *msg;

   public:
    ParseException(const char *message) : std::exception(), msg(message) {}
    const char *getMessage() const {
        return msg;
    }
};