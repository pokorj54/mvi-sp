#include "Qubic.hpp"
#include "utils.hpp"
#include "QubicLikeUtils.hpp"

#include <algorithm>
#include <cmath>

Qubic::QubicBoard Qubic::lines[76];

Qubic::QubicBoard Qubic::pointLines[64][7];

Qubic::Qubic(QubicBoard firstPlayerStones, QubicBoard secondPlayerStones) : firstPlayerStones(firstPlayerStones), secondPlayerStones(secondPlayerStones) {
    // no two stones are at the same cell
    assert((firstPlayerStones & secondPlayerStones) == 0);
    // players have the right amount of stones
    int firstPCount = countOnes(firstPlayerStones);
    int secondPCount = countOnes(secondPlayerStones);
    assert(firstPCount == secondPCount || firstPCount == secondPCount + 1);
}

bool Qubic::operator==(const Qubic &other) const {
    return firstPlayerStones == other.firstPlayerStones && secondPlayerStones == other.secondPlayerStones;
}

bool Qubic::operator!=(const Qubic &other) const {
    return !(*this == other);
}

bool Qubic::operator<=(const Qubic &other) const {
    if (this->firstPlayerStones != other.firstPlayerStones) {
        return this->firstPlayerStones <= other.firstPlayerStones;
    }
    return this->secondPlayerStones <= other.secondPlayerStones;
}

bool Qubic::operator>=(const Qubic &other) const {
    return other <= *this;
}

bool Qubic::operator<(const Qubic &other) const {
    return !(*this >= other);
}

bool Qubic::operator>(const Qubic &other) const {
    return !(*this <= other);
}

GameState Qubic::getState() const {
    if (hasWinningLine(firstPlayerStones)) {
        return FirstWon;
    }
    if (hasWinningLine(secondPlayerStones) || (firstPlayerStones | secondPlayerStones) == (QubicBoard)-1) {  // -1 = 0xff...ff, it is sufficient only to check second player
        return SecondWonOrDraw;
    }
    return Open;
}

GameState Qubic::getState(const Qubic &parent) const {
    QubicBoard first = this->firstPlayerStones ^ parent.firstPlayerStones;
    QubicBoard second = this->secondPlayerStones ^ parent.secondPlayerStones;
    bool played_first = first != 0;
    if (played_first) {
        assert(second == 0);
        return hasWinningLine(this->firstPlayerStones, first, pointLines) ? FirstWon : Open;
    } else {
        assert(first == 0);
        bool draw = (firstPlayerStones | secondPlayerStones) == (QubicBoard)-1;
        return hasWinningLine(this->secondPlayerStones, second, pointLines) | draw ? SecondWonOrDraw : Open;
    }
}

Qubic Qubic::loadGame(istream &is) {
    Qubic game;
    for (int i = 0; i < 64; ++i) {
        char c;
        is >> c;
        if (is.bad() || is.eof()) {
            throw ParseException("Expected more characters");
        }
        game.firstPlayerStones = game.firstPlayerStones << 1;
        game.secondPlayerStones = game.secondPlayerStones << 1;
        switch (c) {
            case 'x':
                game.firstPlayerStones += 1;
                break;
            case 'o':
                game.secondPlayerStones += 1;
                break;
            case '-':
                break;
            default:
                throw ParseException("Unexpected character");
        }
    }
    return game;
}

bool Qubic::firstPlayerPlays() const {
    return countOnes(firstPlayerStones) == countOnes(secondPlayerStones);
}

ostream &operator<<(ostream &os, const Qubic &q) {
    Qubic::QubicBoard bit = 1UL << 63;  // bit that is currently searched
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            for (int k = 0; k < 4; ++k) {
                if ((bit & q.firstPlayerStones) == bit) {
                    os << 'x';
                } else if ((bit & q.secondPlayerStones) == bit) {
                    os << 'o';
                } else {
                    os << '-';
                }
                bit = bit >> 1;
            }
            os << endl;
        }
        os << endl;
    }
    return os;
}

int gradeMoves(int stone_index, Qubic::QubicBoard firstPlayerStones, Qubic::QubicBoard secondPlayerStones, bool plays_first) {
    int score = 100;
    Qubic::QubicBoard nextPlayer = plays_first ? firstPlayerStones : secondPlayerStones;
    Qubic::QubicBoard opponent = plays_first ? secondPlayerStones : firstPlayerStones;
    bool isomd = is_on_main_diagonal(1UL << stone_index);
    int numLines = isomd ? 7 : 4;

    const int score_transformation[16] = {// row is player, column is opponent, -0 marks tha it is not possible
                                          5, 0, 0, 1000,
                                          15, -2, -2, -0,
                                          100, -2, -0, -0,
                                          10000, -0, -0, -0};
    for (int i = 0; i < numLines; ++i) {
        Qubic::QubicBoard line = Qubic::pointLines[stone_index][i];
        int nextPlayerCount = countOnes(nextPlayer & line);
        int opponentCount = countOnes(opponent & line);
        int combined = nextPlayerCount * 4 + opponentCount;
        score += score_transformation[combined];
    }

    (void)firstPlayerStones;
    (void)secondPlayerStones;
    return (isomd == !plays_first) ? score + 10 : score;
}

int Qubic::getChildrenCount() const {
    return 64 - countOnes(firstPlayerStones | secondPlayerStones);
}

vector<Qubic> Qubic::generateChildren() const {
    QubicBoard stones = firstPlayerStones | secondPlayerStones;
    int n_childs = 64 - countOnes(stones);
    bool plays_first = n_childs % 2 == 0;

    vector<pair<QubicBoard, int>> validMoves = vector<pair<QubicBoard, int>>(n_childs);
    QubicBoard stone = 0x1;
    int current_child = 0;
    for (int i = 0; i < 64; ++i) {
        if ((stone & stones) == 0) {
            int points;
            points = gradeMoves(i, firstPlayerStones, secondPlayerStones, plays_first);
            validMoves[current_child++] = {stone, points};
        }
        stone = stone << 1;
    }

    sort(validMoves.begin(), validMoves.end(), [](const pair<QubicBoard, int> &a, const pair<QubicBoard, int> &b) { return a.second > b.second; });

    vector<Qubic> children;
    for (auto &gradedBoard : validMoves) {
        QubicBoard move = gradedBoard.first;
        if (plays_first) {
            children.push_back(Qubic(firstPlayerStones | move, secondPlayerStones));
        } else {
            children.push_back(Qubic(firstPlayerStones, secondPlayerStones | move));
        }
    }
    return children;
}

void Qubic::generateLines() {
    int count = 0;
    // ordinary lines
    // left to right
    generateOffsetedParallelLines(0xf, 4, 16, lines + count);
    count += 16;
    // forward backward
    Qubic::QubicBoard mask = 0x1111;
    for (int i = 0; i < 4; ++i) {
        generateOffsetedParallelLines(mask, 1, 4, lines + count);
        count += 4;
        mask = mask << 16;
    }
    // up down
    generateOffsetedParallelLines(0x0001000100010001, 1, 16, lines + count);
    count += 16;
    // face diagonals
    // face up
    generateOffsetedParallelLines(0x8421, 16, 4, lines + count);
    count += 4;
    generateOffsetedParallelLines(0x1248, 16, 4, lines + count);
    count += 4;
    // face left
    generateOffsetedParallelLines(0x1000010000100001, 1, 4, lines + count);
    count += 4;
    generateOffsetedParallelLines(0x0001001001001000, 1, 4, lines + count);
    count += 4;
    //face forward
    generateOffsetedParallelLines(0x0008000400020001, 4, 4, lines + count);
    count += 4;
    generateOffsetedParallelLines(0x0001000200040008, 4, 4, lines + count);
    count += 4;
    // main diagonals
    generateOffsetedParallelLines(0x8000040000200001, 0, 1, lines + count);
    count += 1;
    generateOffsetedParallelLines(0x1000020000400008, 0, 1, lines + count);
    count += 1;
    generateOffsetedParallelLines(0x0008004002001000, 0, 1, lines + count);
    count += 1;
    generateOffsetedParallelLines(0x0001002004008000, 0, 1, lines + count);
    count += 1;
    assert(count == 76);

    QubicBoard stone = 1;
    for (int i = 0; i < 64; ++i) {
        int current_line = 0;
        for (int j = 0; j < 76; ++j) {
            if ((stone & lines[j]) == stone) {
                pointLines[i][current_line++] = lines[j];
            }
        }
        assert(current_line == (is_on_main_diagonal(stone) ? 7 : 4));
        stone = stone << 1;
    }
}

size_t Qubic::compress(size_t mod) const {
    // move to higher
    // - -> *3 + 0
    // o -> *3 + 1
    // x -> *3 + 2
    size_t result = 0;
    Qubic::QubicBoard stone = 1;
    for (int i = 0; i < 64; ++i) {
        result = result * 3;
        if ((firstPlayerStones & stone) == stone) {
            result += 2;
        } else if ((secondPlayerStones & stone) == stone) {
            result += 1;
        }
        result %= mod;
    }
    return result;
}