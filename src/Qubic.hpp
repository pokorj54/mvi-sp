#pragma once

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <vector>

#include "GameState.hpp"
#include "ParseException.hpp"

using namespace std;

struct Qubic {
    using QubicBoard = uint64_t;

    QubicBoard firstPlayerStones;   // x
    QubicBoard secondPlayerStones;  // o
    Qubic() : firstPlayerStones(0), secondPlayerStones(0) {}
    Qubic(QubicBoard firstPlayerStones, QubicBoard secondPlayerStones);

    bool operator==(const Qubic &other) const;

    bool operator!=(const Qubic &other) const;

    bool operator<=(const Qubic &other) const;

    bool operator>=(const Qubic &other) const;

    bool operator<(const Qubic &other) const;

    bool operator>(const Qubic &other) const;

    GameState getState() const;
    GameState getState(const Qubic &parent) const;
    int getChildrenCount() const;
    vector<Qubic> generateChildren() const;
    bool firstPlayerPlays() const;

    size_t compress(size_t mod) const;
    static const size_t maxCompressedSize = 102;

    /* board mapped to bits (index)

    63 62 61 60
    59 58 57 56
    55 54 53 52
    51 50 49 48
    
    47 46 45 44
    43 42 41 40
    39 38 37 36
    35 34 33 32
    
    31 30 29 28
    27 26 25 24
    23 22 21 20
    19 18 17 16
    
    15 14 13 12
    11 10 09 08
    07 06 05 04
    03 02 01 00
    */
    static Qubic loadGame(istream &is);

    friend std::ostream &operator<<(std::ostream &os, const Qubic &q);

    static QubicBoard lines[76];
    static QubicBoard pointLines[64][7];

    static void generateLines();
};

namespace std {
template <>
struct hash<Qubic> {
    std::size_t operator()(const Qubic &q) const {
        Qubic::QubicBoard rotatedSecondPlayerStones = (q.secondPlayerStones << (sizeof(Qubic::QubicBoard) / 2)) |
                                                      (q.secondPlayerStones >> (sizeof(Qubic::QubicBoard) / 2));
        return std::hash<Qubic::QubicBoard>{}(q.firstPlayerStones) ^ std::hash<Qubic::QubicBoard>{}(rotatedSecondPlayerStones);
    }
};
}  // namespace std
