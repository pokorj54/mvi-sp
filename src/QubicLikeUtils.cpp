#include "QubicLikeUtils.hpp"
#include "utils.hpp"
#include "GameState.hpp"

#include <cmath>
#include <cassert>
#include <vector>

bool hasWinningLine(QubicBoard stones){
    // This is slower idk why
    // for (Qubic::QubicBoard line : Qubic::lines) {
    //     if (line == (line & stones)) {
    //         return true;
    //     }
    // }
    // return false;

    bool result = false;

    // ordinary lines
    // left to right
    result = result || checkMask<QubicBoard>(stones, 0xf, 4, 16);
    // forward backward
    QubicBoard mask = 0x1111;
    for (int i = 0; i < 4; ++i) {
        result = result || checkMask<QubicBoard>(stones, mask, 1, 4);
        mask = mask << 16;
    }
    // up down
    result = result || checkMask<QubicBoard>(stones, 0x0001000100010001, 1, 16);
    // face diagonals
    // face up
    result = result || checkMask<QubicBoard>(stones, 0x8421, 16, 4);
    result = result || checkMask<QubicBoard>(stones, 0x1248, 16, 4);
    // face left
    result = result || checkMask<QubicBoard>(stones, 0x1000010000100001, 1, 4);
    result = result || checkMask<QubicBoard>(stones, 0x0001001001001000, 1, 4);
    //face forward
    result = result || checkMask<QubicBoard>(stones, 0x0008000400020001, 4, 4);
    result = result || checkMask<QubicBoard>(stones, 0x0001000200040008, 4, 4);
    // main diagonals
    result = result || checkMask<QubicBoard>(stones, 0x8000040000200001, 0, 1);
    result = result || checkMask<QubicBoard>(stones, 0x1000020000400008, 0, 1);
    result = result || checkMask<QubicBoard>(stones, 0x0008004002001000, 0, 1);
    result = result || checkMask<QubicBoard>(stones, 0x0001002004008000, 0, 1);
    return result;
}


bool is_on_main_diagonal(QubicBoard move){
    assert(countOnes(move) == 1);
    bool is_main_diagonal = false;
    int cells_on_main_diagonal[16] = {63, 60, 51, 48, 42, 41, 38, 37, 26, 25, 22, 21, 15, 12, 3, 0};
    for (int i = 0; i < 16; ++i) {
        if ((move >> cells_on_main_diagonal[i]) == 1) {
            is_main_diagonal = true;
            break;
        }
    }
    return is_main_diagonal;
}


void generateOffsetedParallelLines(QubicBoard line, int step, int times, QubicBoard *output_array) {
    for (int i = 0; i < times; ++i) {
        *output_array = line;
        ++output_array;
        line = line << step;
    }
}


bool hasWinningLine(QubicBoard stones, QubicBoard last_move, const QubicBoard pointLines[64][7]) {
    int stone_index = log2(last_move);

    bool isomd = is_on_main_diagonal(1UL << stone_index);
    int numLines = isomd ? 7 : 4;
    for (int i = 0; i < numLines; ++i) {
        QubicBoard line = pointLines[stone_index][i];

        if ((line & stones) == line) {
            return true;
        }
    }
    return false;
}
