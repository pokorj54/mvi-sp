#pragma once
#include <cstdint>

using QubicBoard = uint64_t;

bool hasWinningLine(QubicBoard stones);

bool is_on_main_diagonal(QubicBoard move);

void generateOffsetedParallelLines(QubicBoard line, int step, int times, QubicBoard *output_array);

bool hasWinningLine(QubicBoard stones, QubicBoard last_move, const QubicBoard pointLines[64][7]);
