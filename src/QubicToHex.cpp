#include "Qubic.hpp"

int main(void) {
    Qubic game = Qubic::loadGame(cin);
    std::cout << std::hex;
    std::cout << game.firstPlayerStones << " " << game.secondPlayerStones << endl;
    return 0;
}