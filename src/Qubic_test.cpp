#include "Qubic.hpp"
#include "utils.hpp"
#include "QubicLikeUtils.hpp"

#include <assert.h>
#include <iomanip>

int main(void) {
    Qubic::generateLines();
    assert(hasWinningLine(0x1111));
    assert(hasWinningLine(0x0000111100000000));
    assert(hasWinningLine(0x1111000000000000));
    assert(hasWinningLine(0x4444));
    assert(hasWinningLine(0x0000222200000000));
    assert(hasWinningLine(0x8888000000000000));
    assert(hasWinningLine(0xf));
    assert(hasWinningLine(0xf000));
    assert(hasWinningLine(0xf000000000000000));
    assert(hasWinningLine(0x1000100010001000));
    assert(hasWinningLine(0x300020006000A000));
    assert(hasWinningLine(0xffff));
    assert(hasWinningLine(0x9abc));
    assert(hasWinningLine(0x4000066006600004));

    assert(!hasWinningLine(0));
    assert(!hasWinningLine(0x1));
    assert(!hasWinningLine(0x11101));
    assert(!hasWinningLine(0x1000100010000));
    assert(!hasWinningLine(0x11110));
    assert(!hasWinningLine(0x888088808880000));
    assert(!hasWinningLine(0xb28e53e2b94347d1));
    assert(!hasWinningLine(0x4d71ac1d46bcb82e));

    assert(Qubic(0x1111, 0x4060).getState() == FirstWon);
    assert(Qubic(0x07770010000543210, 0xabcdef).getState() == SecondWonOrDraw);
    assert(Qubic(0, 0).getState() == Open);
    assert(Qubic(0x1002003004005006, 0x0330000000000330).getState() == Open);
    assert(Qubic(0xb28e53e2b94347d1, 0x4d71ac1d46bcb82e).getState() == SecondWonOrDraw);  // draw

    assert(Qubic(0x1111, 0x4060).getState(Qubic(0x0111, 0x4060)) == FirstWon);
    assert(Qubic(0x07770010000543210, 0xabcdef).getState(Qubic(0x07770010000543210, 0xabcde7)) == SecondWonOrDraw);
    assert(Qubic(1, 0).getState(Qubic(0, 0)) == Open);
    assert(Qubic(0x1002003004005006, 0x0330000000000330).getState(Qubic(0x0002003004005006, 0x0330000000000330)) == Open);
    assert(Qubic(0xb28e53e2b94347d1, 0x4d71ac1d46bcb82e).getState(Qubic(0xb28e53e2b94347d1, 0x4971ac1d46bcb82e)) == SecondWonOrDraw);  // draw

    Qubic empty;
    vector<Qubic> children = empty.generateChildren();
    assert(children.size() == 64);
    assertUniqueElements(children);
    assertAllChildState(children, Open);

    Qubic cube_position = Qubic(0x7770777077700000, 0x8887888788877770);
    children = cube_position.generateChildren();
    assert(children.size() == 10);
    assertUniqueElements(children);
    assertAllChildState(children, FirstWon);

    assert(is_on_main_diagonal(0x1));
    assert(!is_on_main_diagonal(0x2));
    assert(!is_on_main_diagonal(0x4));
    assert(is_on_main_diagonal(0x8));
    assert(is_on_main_diagonal(0x8000000000000000));
    assert(is_on_main_diagonal(0x1000000000000000));
    assert(is_on_main_diagonal(0x0008000000000000));
    assert(is_on_main_diagonal(0x0001000000000000));

    return 0;
}