#pragma once

#include <cstdint>
#include <cstdlib>

struct SaturatedUInt {
    const static uint INFTY = UINT32_MAX;

    SaturatedUInt(uint value) : val(value) {}
    bool operator==(const SaturatedUInt &other) const {
        return val == other.val;
    }
    bool operator!=(const SaturatedUInt &other) const {
        return val != other.val;
    }
    bool operator<(const SaturatedUInt &other) const {
        return val < other.val;
    }
    bool operator>(const SaturatedUInt &other) const {
        return val > other.val;
    }
    bool operator<=(const SaturatedUInt &other) const {
        return val <= other.val;
    }
    bool operator>=(const SaturatedUInt &other) const {
        return val >= other.val;
    }
    SaturatedUInt operator+(const SaturatedUInt &other) const {
        uint res = val + other.val;
        if (res < val)
            return INFTY;
        return res;
    }
    SaturatedUInt operator-(const SaturatedUInt &other) const {
        return (val < other.val) ? 0 : val - other.val;
    }
    explicit operator uint() const {
        return val;
    }

   private:
    uint val;
};