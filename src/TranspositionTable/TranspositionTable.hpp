#pragma once

#include <map>

#include "../GameState.hpp"

template <typename Game>
class TranspositionTable {
    std::map<Game, GameState> tt;

   public:
    void add(const Game& game, GameState state) {
        tt.insert(make_pair(game, state));
    }

    GameState getGameState(const Game& game) const {
        if (tt.count(game) > 0) {
            return tt.at(game);
        }
        return Open;
    }
    size_t size() const {
        return tt.size();
    }
};