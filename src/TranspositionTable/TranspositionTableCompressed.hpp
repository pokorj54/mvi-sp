#pragma once

#include "../GameState.hpp"

// does not have to remember all positions, it may replace elements
template <typename Game, size_t N>
class TranspositionTableCompressed {
    pair<Game, GameState> tt[N];
    size_t count = 0;

   public:
    void add(const Game& game, GameState state) {
        tt[game.compress(N)] = make_pair(game, state);
        ++count;
    }
    GameState getGameState(const Game& game) const {
        const pair<Game, GameState>& p = tt[game.compress(N)];
        if (p.first == game) {
            return p.second;
        }
        return Open;
    }
    size_t size() const {
        return count;
    }
};