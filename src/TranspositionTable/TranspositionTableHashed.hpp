#pragma once

#include <unordered_map>
#include <vector>
#include <cassert>

#include "../GameState.hpp"

template <typename Game>
class TranspositionTableHashed {
    std::unordered_map<Game, std::pair<GameState, double>> tt;
    size_t max_size;

   public:
    TranspositionTableHashed(size_t max_size = SIZE_MAX):max_size(max_size){}
    void add(const Game& game, GameState state, double score = 0) {
        if(tt.size() > max_size){
            eraseWorst(0.2);
        }
        tt.insert({game, {state, score}});
    }
    GameState getGameState(const Game& game) const {
        if (tt.count(game) > 0) {
            return tt.at(game).first;
        }
        return Open;
    }
    double getVal(const Game& game) const {
        if (tt.count(game) > 0) {
            return tt.at(game).second;
        }
        assert(0 && "Value not in tt");
    }

    void eraseWorst(double perc){
        double minval = (*tt.begin()).second.second;
        double maxval = minval;
        for( const auto & p : tt){
            minval = std::min(minval, p.second.second);
            maxval = std::max(maxval, p.second.second);
        }
        double treshold = minval + perc*(maxval-minval);
        std::vector<Game> to_delete;
        for( const auto & p : tt){
            if(p.second.second <= treshold){
                to_delete.push_back(p.first);
            }
        }
        for(const Game & game : to_delete){
            tt.erase(game);
        }
    }
    size_t size() const {
        return tt.size();
    }
};