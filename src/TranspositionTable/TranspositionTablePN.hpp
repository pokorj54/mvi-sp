#pragma once

#include <unordered_map>

#include "../SaturatedUInt.hpp"

template <typename Game>
class TranspositionTablePN {
    std::unordered_map<Game, std::pair<SaturatedUInt, SaturatedUInt>> tt;

   public:
    void update(const Game& game, const std::pair<SaturatedUInt, SaturatedUInt>& pn_dn) {
        if (tt.count(game) > 0) {
            tt.erase(game);
        }
        tt.insert(std::make_pair(game, pn_dn));
    }

    std::pair<SaturatedUInt, SaturatedUInt> getPnDn(const Game& game) const {
        if (tt.count(game) > 0) {
            return tt.at(game);
        }
        return std::make_pair(1, 1);
    }
};
