#include "TranspositionTablePN.hpp"

#include <cassert>

#include "../Qubic.hpp"

int main(void) {
    TranspositionTablePN<Qubic> tt;

    //inserting first game
    Qubic game(0x1111, 0x0222);
    tt.update(game, {3, 1});
    std::pair<SaturatedUInt, SaturatedUInt> pndn = tt.getPnDn(game);
    assert(pndn.first == 3);
    assert(pndn.second == 1);

    // inserting another game
    Qubic game2(0x6600, 0x0066);
    tt.update(game2, {4, 6});
    pndn = tt.getPnDn(game2);
    assert(pndn.first == 4);
    assert(pndn.second == 6);

    // checking again the first game
    pndn = tt.getPnDn(game);
    assert(pndn.first == 3);
    assert(pndn.second == 1);

    // updating the first game
    tt.update(game, {37, 11});
    pndn = tt.getPnDn(game);
    assert(pndn.first == 37);
    assert(pndn.second == 11);

    // new game
    pndn = tt.getPnDn(Qubic(0, 0));
    assert(pndn.first == 1);
    assert(pndn.second == 1);
    return 0;
}