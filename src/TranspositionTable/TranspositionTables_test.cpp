#include <cassert>

#include "../Qubic.hpp"
#include "TranspositionTable.hpp"
#include "TranspositionTableCompressed.hpp"
#include "TranspositionTableHashed.hpp"
template <typename T>
void testTTOnQubic(T& tt) {
    Qubic game(0x1111, 0x0222);
    tt.add(game, FirstWon);
    assert(tt.getGameState(game) == FirstWon);
    assert(tt.getGameState(Qubic(0, 0)) == Open);
    Qubic game2(0x1110, 0x0220);
    assert(tt.getGameState(game2) == Open);
    tt.add(game2, FirstWon);
    assert(tt.getGameState(game2) == FirstWon);
    assert(tt.size() == 2);
}

int main(void) {
    TranspositionTable<Qubic> tt;
    testTTOnQubic(tt);
    TranspositionTableHashed<Qubic> tt_Hashed;
    testTTOnQubic(tt_Hashed);
    // does not have to remember all positions
    TranspositionTableCompressed<Qubic, 10000> tt_compressed;
    testTTOnQubic(tt_compressed);
    return 0;
}