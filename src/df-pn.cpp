#include "df-pn.hpp"

#include "TranspositionTable/TranspositionTablePN.hpp"

int df_pn_count = 0;
TranspositionTablePN<Qubic> pn_tt;

void RetrieveProofandDisproofNumbers(const Qubic &game, SaturatedUInt &pn, SaturatedUInt &dn) {
    pair<SaturatedUInt, SaturatedUInt> p = pn_tt.getPnDn(game);
    pn = p.first;
    dn = p.second;
}

void SaveProofandDisproofNumbers(const DFPNNode &n, SaturatedUInt phi, SaturatedUInt delta) {
    pn_tt.update(n.game, make_pair(phi, delta));
}
// Compute the smallest delta of N’s children
SaturatedUInt deltaMin(const DFPNNode &N) {
    SaturatedUInt min = SaturatedUInt::INFTY;
    for (const Qubic &child : N.children) {
        SaturatedUInt phi = 0, delta = 0;  //will be changed immediatelly
        RetrieveProofandDisproofNumbers(child, phi, delta);
        min = std::min(min, delta);
    }
    return min;
}
// Compute sum of phi of N’s children
SaturatedUInt phiSum(const DFPNNode &N) {
    SaturatedUInt sum = 0;
    for (const Qubic &child : N.children) {
        SaturatedUInt phi = 0, delta = 0;  //will be changed immediatelly
        RetrieveProofandDisproofNumbers(child, phi, delta);
        sum = sum + phi;
    }
    return sum;
}

// Select the best child
DFPNNode SelectChild(DFPNNode &N, SaturatedUInt &phic, SaturatedUInt &delta2) {
    Qubic Cbest;
    SaturatedUInt deltac = SaturatedUInt::INFTY;
    phic = SaturatedUInt::INFTY;
    for (const Qubic &child : N.children) {
        SaturatedUInt phi = 0, delta = 0;  //will be changed immediatelly
        RetrieveProofandDisproofNumbers(child, phi, delta);
        // Store the smallest and second
        // smallest delta in deltac and delta2
        if (delta < deltac) {
            Cbest = child;
            delta2 = deltac;
            phic = phi;
            deltac = delta;
        } else if (delta < delta2)
            delta2 = delta;
        if (phi == SaturatedUInt::INFTY)
            return DFPNNode(Cbest);
    }
    // phi and delta are assigned out of this method
    return DFPNNode(Cbest);
}

// Perform search with thresholds
void MID(DFPNNode &N) {
    ++df_pn_count;
    // Terminal leaf
    GameState gameState = N.game.getState();
    if (gameState != Open) {
        N.setProofAndDisproofNumbers(gameState);
        // Store (dis)proven DFPNNode
        SaveProofandDisproofNumbers(N, N.phi(), N.delta());
        return;
    }
    N.expand();
    // Continue search until satisfying
    // the termination condition
    while (N.phi() > deltaMin(N) && N.delta() > phiSum(N)) {
        SaturatedUInt phic = 0, delta2 = 0;  // will be changed immediatelly
        DFPNNode Cbest = SelectChild(N, phic, delta2);
        // Update thresholds
        Cbest.phi() = N.delta() + phic - phiSum(N);
        Cbest.delta() = std::min(N.phi(), delta2 + 1);
        MID(Cbest);
    }
    // Store search results
    N.phi() = deltaMin(N);
    N.delta() = phiSum(N);
    SaveProofandDisproofNumbers(N, N.phi(), N.delta());
}

// Set up for the root DFPNNode
// Note that the root is an OR DFPNNode
void df_pn(DFPNNode &R) {
    R.phi() = SaturatedUInt::INFTY;
    R.delta() = SaturatedUInt::INFTY;
    MID(R);
    cerr << df_pn_count << endl;
    // if (R.delta() == SaturatedUInt::INFTY)
    //     return 1;
    // else
    //     return 0;
}

DFPNNode::DFPNNode(const Qubic &game, bool isAndDFPNNode) : pn(SaturatedUInt::INFTY), dn(SaturatedUInt::INFTY), game(game), isAndDFPNNode(isAndDFPNNode) {}

DFPNNode::DFPNNode(const Qubic &game) : DFPNNode(game, !game.firstPlayerPlays()) {}

void DFPNNode::expand() {
    children = this->game.generateChildren();
}

void DFPNNode::setProofAndDisproofNumbers(GameState state) {
    switch (state) {
        case SecondWonOrDraw:
            this->pn = SaturatedUInt::INFTY;
            this->dn = 0;
            break;
        case FirstWon:
            this->pn = 0;
            this->dn = SaturatedUInt::INFTY;
            break;
        case Open:
            this->pn = 1;
            this->dn = 1;
            break;
        default:
            throw std::invalid_argument("Unknown gamestate");
    }
}

GameState DFPNNode::getGameState() const {
    if (pn == 0) {
        return FirstWon;
    } else if (dn == 0) {
        return SecondWonOrDraw;
    }
    return Open;
}

ostream &operator<<(ostream &os, const DFPNNode &n) {
    os << "Type: " << (n.isAndDFPNNode ? "AND" : "OR") << endl;
    os << "pn: " << (uint)n.pn << endl;
    os << "dn: " << (uint)n.dn << endl;
    os << "State: " << GameStateToString(n.getGameState()) << endl;
    os << n.game;
    return os;
}