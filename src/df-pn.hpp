#include <cstdint>
#include <vector>

#include "Qubic.hpp"
#include "SaturatedUInt.hpp"

// from GAME-TREE SEARCH USING PROOF NUMBERS: THE FIRST TWENTY YEARS

struct DFPNNode;

void df_pn(DFPNNode &root);

struct DFPNNode {
   private:
    DFPNNode(const Qubic &game, bool isAndDFPNNode);

   public:
    DFPNNode(const Qubic &game);
    SaturatedUInt pn, dn;
    // pn = 0 - win for 1st player
    // dn = 0 - draw or win for 2nd player
    const Qubic game;
    const bool isAndDFPNNode;
    vector<Qubic> children;

    bool operator==(const DFPNNode &other) const;

    bool operator!=(const DFPNNode &other) const;

    // dn if AND DFPNNode
    // pn if OR DFPNNode
    SaturatedUInt &phi() {
        return isAndDFPNNode ? dn : pn;
    }

    // pn if AND DFPNNode
    // dn if OR DFPNNode
    SaturatedUInt &delta() {
        return isAndDFPNNode ? pn : dn;
    }

    bool isExpanded() const;

    GameState getGameState() const;
    void expand();
    void setProofAndDisproofNumbers(GameState state);

    friend std::ostream &operator<<(std::ostream &os, const DFPNNode &n);
};
