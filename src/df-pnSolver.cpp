#include <iomanip>
#include <iostream>

#include "GameState.hpp"
#include "PNS.hpp"
#include "Qubic.hpp"
#include "df-pn.hpp"

int main(void) {
    Qubic::generateLines();
    Qubic game;
    try {
        game = Qubic::loadGame(std::cin);
    } catch (const ParseException& e) {
        std::cerr << e.getMessage() << endl;
        throw;
    }

    DFPNNode root(game);
    df_pn(root);
    cout << GameStateToString(root.getGameState()) << endl;
    return 0;
}