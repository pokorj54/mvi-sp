#pragma once

#include "GameState.hpp"
#include <vector>

template <typename T>
void assertUniqueElements(std::vector<T>& v) {
    for (size_t i = 0; i < v.size(); ++i) {
        for (size_t j = 0; j < v.size(); ++j) {
            assert(i == j || v[i] != v[j]);
        }
    }
}

template <typename GAME>
void assertAllChildState(std::vector<GAME> & v, GameState state) {
    for (GAME child : v) {
        assert(child.getState() == state);
    }
}


/*
 * Checks whether @stones are equal to @mask, that can be shifted by @step up to @times 
 */
template <typename Number>
bool checkMask(Number stones, Number mask, int step, int times) {
    for (int i = 0; i < times; ++i) {
        if (mask == (mask & stones)) {
            return true;
        }
        mask = mask << step;
    }
    return false;
}

/*
 * Counts number of bits set to 1
 */
template <typename Number>
int countOnes(Number bits) {
    // TODO more generic? - https://stackoverflow.com/a/109025
    return __builtin_popcountll(bits);
}
